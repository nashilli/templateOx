﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Settings;
using Assets.Scripts.App;

namespace Assets.Scripts.Metrics{
    [Serializable]
	public class MetricsModel 
	{
        private const float MAX_SCORE = 10000;
        private const float MIN_SCORE = 500;
		public List<List<GameMetrics>> metrics;

		public MetricsModel(){
			CreateLevelMetrics ();
		}

		private void CreateLevelMetrics(){

			metrics = new List<List<GameMetrics>>();
			for (int area = 0; area < 20; area++) {
                metrics.Add (new List<GameMetrics>());
			}
		}

		public bool LevelHasMetrics (int level){
			return metrics[level].Count > 0;
		}

        internal void DiscardCurrentMetrics()
        {
            int game = AppController.GetController().GetCurrentGame();
            metrics[game].RemoveAt(metrics[game].Count - 1);
        }

        internal void GameFinished(int lapsedSeconds, int minSeconds, int pointsPerSecond, int pointsPerError){
            CalculateFinalScore(lapsedSeconds, minSeconds, pointsPerSecond, pointsPerError);
            GetCurrentMetrics().SetStars(CalculeteStars());
        }

        internal List<List<GameMetrics>> GetMetrics()
        {
            return metrics;
        }

        internal void AddWrongAnswer() {
            GetCurrentMetrics().AddWrongAnswer();
        }

        internal void AddRightAnswer()
        {
            GetCurrentMetrics().AddRightAnswer();
        }   

        internal GameMetrics GetCurrentMetrics()
        {
            int game = AppController.GetController().GetCurrentGame();
            return metrics[game][metrics[game].Count - 1];
        }

        internal float GetMaxScore()
        {
            return MAX_SCORE;
        }

        internal List<int> GetLevelIndexes(int currentPage, int maxRows)
        {
            List<int> myList = new List<int>(maxRows);
            int initIndex = currentPage * maxRows;
            for (int i = initIndex; i < initIndex + maxRows && i < metrics.Count && initIndex >= 0; i++)
            {
                myList.Add(i);
            }
            return myList;
        }

        internal GameMetrics GetBestMetric(int game){
            if (metrics[game].Count == 0) return null;
            GameMetrics max = metrics[game][0];
            for (int i = 1; i < metrics[game].Count; i++){
                if (metrics[game][i].GetScore() > max.GetScore()) {max = metrics[game][i];}
            }
            return max;
        }
      
        internal void GameStarted(){
            int game = AppController.GetController().GetCurrentGame();
            metrics[game].Add(new GameMetrics(game));
        }       

        private void CalculateFinalScore(int lpasedSeconds, int minSeconds, int pointsPerSecond, int pointsPerError){
            GetCurrentMetrics().SetLapsedSeconds(lpasedSeconds);
            GetCurrentMetrics().SetScore(MAX_SCORE - pointsPerSecond * (lpasedSeconds < minSeconds ? 0 : lpasedSeconds - minSeconds) - GetCurrentMetrics().GetWrongAnswers() * pointsPerError);
            if (GetCurrentMetrics().GetScore() < MIN_SCORE) GetCurrentMetrics().SetScore(MIN_SCORE);
           
        }

        public float GetFinalScore(){
            return GetCurrentMetrics().GetScore();
        }
     
        private int CalculeteStars(){
            float percentage = (GetCurrentMetrics().GetScore() + 0f) / (MAX_SCORE + 0f);

            if (percentage > 0.85){
                return 3;
            } else if(percentage > 0.69)
            {
                return 2;
            } else if(percentage > 0.39)
            {
                return 1;
            } else
            {
                return 0;
            }
        }   
    }
}

