﻿using UnityEngine;
using Assets.Scripts.Settings;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.Metrics
{
    public class MetricsController : MonoBehaviour
    {
        private static MetricsController metricsController;
        public MetricsModel metricsModel;

        void Awake()
        {
            if (metricsController == null) metricsController = this;
            else if (metricsController != this) Destroy(gameObject);
            DontDestroyOnLoad(gameObject);
            metricsModel = new MetricsModel();
        }

        internal GameMetrics GetCurrentMetrics()
        {
            return metricsModel.GetCurrentMetrics();
        }

        internal List<List<GameMetrics>> GetMetrics()
        {
            return metricsModel.GetMetrics();
        }

        internal void DiscardCurrentMetrics()
        {
            metricsModel.DiscardCurrentMetrics();
        }

        internal List<GameMetrics> GetMetricsByGame(int game)
        {
            return metricsModel.GetMetrics()[game];
        }

        public void GameStart()
        {
            metricsModel.GameStarted();
            Timer.GetTimer().InitTimer();
        }

        public void GameFinished(int minSeconds, int pointsPerSecond, int pointsPerError)
        {
            Timer.GetTimer().FinishTimer();
            metricsModel.GameFinished(Timer.GetTimer().GetLapsedSeconds(), minSeconds, pointsPerSecond, pointsPerError);
            saveToDisk();
        }

        internal GameMetrics GetBestMetric(int game)
        {
            return metricsModel.GetBestMetric(game);
        }

        public void LogAnswer(bool isCorrect)
        {
            if (isCorrect) { metricsModel.AddRightAnswer(); } else { metricsModel.AddWrongAnswer(); }
        }

        internal float GetMaxScore()
        {
            return metricsModel.GetMaxScore();
        }

        private void saveToDisk()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/" + SettingsController.GetController().GetUsername() + ".dat");
            bf.Serialize(file, metricsModel);
            file.Close();
        }

        public void LoadFromDisk()
        {
            if (File.Exists(Application.persistentDataPath + "/" + SettingsController.GetController().GetUsername() + ".dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/" + SettingsController.GetController().GetUsername() + ".dat", FileMode.Open);
                metricsModel = (MetricsModel)bf.Deserialize(file);
                file.Close();
            }
            else
            {
                metricsModel = new MetricsModel();
            }
        }

        internal static MetricsController GetController()
        {
            return metricsController;
        }
    }
}