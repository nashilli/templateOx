﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts.Metrics
{

    [Serializable]
    public class GameMetrics{
        private const int VERSION = 1;
        private int lapsedSeconds, rightAnswers, wrongAnswers, stars, hints;
        private float score;
        private int index;
        private string date;

        public GameMetrics(int index){
            this.index = index;
            stars = 0;
            lapsedSeconds = 0;
            rightAnswers = 0;
            wrongAnswers = 0;
            score = 0;
            hints = 0;
            date = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
        }     

        internal void Reset(){
            stars = 0;
            lapsedSeconds = 0;
            rightAnswers = 0;
            wrongAnswers = 0;
            score = 0;
            hints = 0;
        }    

        internal int GetStars(){
            return stars;
        }

        internal void AddWrongAnswer()
        {
            this.wrongAnswers++;
        }

        internal void SetStars(int stars)
        {
            this.stars = stars;
        }

        internal void AddRightAnswer()
        {
            this.rightAnswers++;
        }

        internal void AddHint()
        {
            hints++;
        }

        internal float GetHints()
        {
            return hints;
        }

        internal void SetHints(int hints)
        {
            this.hints = hints;
        }

        internal float GetScore()
        {
            return score;
        }

        internal void SetLapsedSeconds(int lapsedSeconds)
        {
            this.lapsedSeconds = lapsedSeconds;
        }

        internal int GetWrongAnswers()
        {
            return wrongAnswers;
        }

        internal void SetScore(float score)
        {
            this.score = score;
        }

        internal int GetRightAnswers()
        {
            return rightAnswers;
        }

        internal string GetDate()
        {
            return date;
        }

        internal void SetDate(string date)
        {
            this.date = date;
        }

        internal int GetLapsedSeconds()
        {
            return lapsedSeconds;
        }

        internal void SetRightAnswers(int rightAnswers)
        {
            this.rightAnswers = rightAnswers;
        }

        internal void SetWrongAnswers(int wrongAnswers)
        {
            this.wrongAnswers = wrongAnswers;
        }     

        internal int GetIndex()
        {
            return index;
        }
        
    }
}