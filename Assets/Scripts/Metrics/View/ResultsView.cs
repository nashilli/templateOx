﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Scripts.Settings;
using Assets.Scripts.App;
using Assets.Scripts.Sound;
using System;

namespace Assets.Scripts.Metrics.View
{
    public class ResultsView : MonoBehaviour
    {
        public GameObject metricsRowPrefab;
        public Text title;
        public GameObject metricsPanel;
        private List<MetricsRow> viewRaws;
        

        void Start(){
            title.text = SettingsController.GetController().GetLanguage() == 0 ? "RESULTADOS" : "RESULTS";
            viewRaws = new List<MetricsRow>();
        }

        private void ShowAllMetrics()
        {
            List<List<GameMetrics>> metrics = MetricsController.GetController().GetMetrics();
            for (int i = 0; i < metrics.Count; i++)
            {
                AddMetricRow(i);
            }            
        }
     

        private void AddMetricRow(int game)
        {
            GameMetrics gameMetric = MetricsController.GetController().GetBestMetric(game);
            GameObject row = Instantiate(metricsRowPrefab);
            viewRaws.Add(row.GetComponent<MetricsRow>());

            if (gameMetric == null)
            {
                row.GetComponent<MetricsRow>().SetActivity(AppController.GetController().GetActivityName( game));
                row.GetComponent<MetricsRow>().SetScore(0);
                row.GetComponent<MetricsRow>().SetStars(0);
                row.GetComponent<MetricsRow>().SetIndex(-1);
            } else
            {
                row.GetComponent<MetricsRow>().SetActivity(AppController.GetController().GetActivityName(game));
                row.GetComponent<MetricsRow>().SetScore(gameMetric.GetScore());
                row.GetComponent<MetricsRow>().SetStars(gameMetric.GetStars());
                row.GetComponent<MetricsRow>().SetIndex(gameMetric.GetIndex());
            }
            
            FitRowToPanel(row);
        }

        private void FitRowToPanel(GameObject child)
        {
            child.transform.SetParent(metricsPanel.transform, true);
            child.transform.localPosition = Vector3.zero;
            child.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            child.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            child.transform.localScale = Vector3.one;
        }

        public void OnClickCrossBtn(){
            PlaySoundClick();
            ViewController.GetController().LoadMainMenu();
        }     

        private void PlaySoundClick()
        {
            SoundController.GetController().PlayClickSound();
        }

        private void updateMetricRows(){
            /*          
            currentRawsToViewDetails.RemoveAll(e => true);
            List<int> activities = MetricsManager.instance.metricsModel.GetLevelIndexes(currentPage, MAX_ROWS);
            currentRawsToViewDetails.AddRange(activities);
            int i = 0;
            for (; i < MAX_ROWS && i < activities.Count; i++){
                updateRow(MetricsManager.instance.metricsModel.GetBestMetric(activities[i]), i, activities[i]);
                raws[i].Show();
            }
            for(; i < MAX_ROWS; i++){
                raws[i].Hide();
            }     
            */   
            
        }

        private void updateRow(GameMetrics gameMetrics, int rowIndex, int activity){
            /*
            raws[rowIndex].setActivity(AppController.instance.appModel.GetTitleFromIndex(activity));

            if (gameMetrics != null){
                raws[rowIndex].setScore(gameMetrics.score);
                raws[rowIndex].setStars(gameMetrics.stars);
                raws[rowIndex].getViewDetailsBtn().enabled = true;
            } else{
                raws[rowIndex].setScore(0);
                raws[rowIndex].setStars(0);
                raws[rowIndex].getViewDetailsBtn().enabled = false;
            } 
            */
        }

        public void OnClickViewDetails(int index){
            PlaySoundClick();
            //ViewDetails(currentRawsToViewDetails[index]);
        }
            
    }
}