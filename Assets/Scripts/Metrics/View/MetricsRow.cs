﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Assets.Scripts.Metrics.View
{

    public class MetricsRow : MonoBehaviour
    {
        [SerializeField]
        private Image icon;
        [SerializeField]
        private Text activityName;
        [SerializeField]
        private Text score;
        [SerializeField]
        private List<Image> stars;
        private int indexGame;


        public void SetActivity(string activityName)
        {
            this.activityName.text = activityName;
        }

        public void SetIcon(Sprite icon)
        {
            this.icon.sprite = icon;
        }

        public void SetScore(float currentScore)
        {
            this.score.text = (currentScore == 0 ? "-" : "" + currentScore);
        }

        public void SetStars(int currentStars)
        {

            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].gameObject.SetActive(i < currentStars);
            }
        }

        public void OnClickViewDetails()
        {
            MetricsView.GetMetricsView().ShowDetailsOf(indexGame);
        }

     

        internal void SetIndex(int index)
        {
            this.indexGame = index;
        }

        
    }
}