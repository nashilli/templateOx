﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts.Metrics.View
{

    public class MetricsView : MonoBehaviour
    {

        private static MetricsView metricsView;

        public DetailsView details;
        public ResultsView results;

        void Awake()
        {
            if (metricsView == null) metricsView = this;
            else if (metricsView != this) Destroy(this);
        }

        void Start()
        {
            ShowResults();
            HideDetails();
        }

        private void HideDetails()
        {
            details.gameObject.SetActive(false);
        }               

        internal void ShowDetailsOf(int idGame)
        {
            Debug.Log("TODO - DETAILS OF " + idGame);
        }

        internal void ShowResults()
        {
            results.gameObject.SetActive(true);
        }

        public static MetricsView GetMetricsView()
        {
            return metricsView;
        }

        
    }
}