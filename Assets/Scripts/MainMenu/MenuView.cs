﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Settings;
using Assets.Scripts.Sound;

namespace Assets.Scripts.MainMenu {
    public class MenuView : MonoBehaviour {    

        void Start() {
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:                  
                    break;
                default:
                    break;
            }
        }

        public void OnClickSettings()
        {
            ClickSound();
            MainMenuController.GetController().ShowSettings();
        }

        public void OnClickMetrics()
        {
            ClickSound();
            MainMenuController.GetController().ShowMetrics();
        }

        public void ClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }
    }
}
