﻿using Assets.Scripts.Settings;
using UnityEngine;
using Assets.Scripts.Sound;

namespace Assets.Scripts.MainMenu
{

    public class GamePreview : MonoBehaviour
    {
   
        void OnEnable()
        {
            UpdateTexts();
        }


        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:                                 
                    break;
                default:                              
                    break;
            }
        }

        public void OnClickBackBtn()
        {
            ClickSound();
            MainMenuController.GetController().ShowMenu();
            gameObject.SetActive(false);
        }     

        internal void ClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }

    }
}
