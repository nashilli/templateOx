﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.App;
using Assets.Scripts.Metrics;
using Assets.Scripts.Settings;
using Assets.Scripts.Sound;
using System;

public class InGameMenuView : MonoBehaviour {

    public Text mainMenuLabel, instructionsLabel, restartGameLabel, backToGameLabel;

    void OnEnable()
    {
        UpdateLanguage();
    }

    private void UpdateLanguage()
    {
        switch (SettingsController.GetController().GetLanguage())
        {
            case 0:
                mainMenuLabel.text = "Menú principal";
                instructionsLabel.text = "Instrucciones";
                restartGameLabel.text = "Comenzar de nuevo";
                backToGameLabel.text = "Volver al juego";
                break;
            case 1:
                mainMenuLabel.text = "Main menu";
                instructionsLabel.text = "Instructiones";
                restartGameLabel.text = "Restart game";
                backToGameLabel.text = "Back to game";
                break;
        }
    }

    public void OnMainMenuClic(){
        PlayClickSound();
        MetricsController.GetController().DiscardCurrentMetrics();
        ViewController.GetController().LoadMainMenu();
        ViewController.GetController().HideInGameMenu();
    }

    public void OnInstructionsClic(){
        PlayClickSound();
        ViewController.GetController().HideInGameMenu();
        ViewController.GetController().ShowInstructions();    
    }

    public void OnClickRestartGame()
    {
        PlayClickSound();
        MetricsController.GetController().DiscardCurrentMetrics();
        ViewController.GetController().RestartCurrentGame();
        ViewController.GetController().HideInGameMenu();

    }

    public void OnClicBackToGame()
    {
        PlayClickSound();
        ViewController.GetController().HideInGameMenu();

    }

    private void PlayClickSound()
    {
        SoundController.GetController().PlayClickSound();
    }
}
