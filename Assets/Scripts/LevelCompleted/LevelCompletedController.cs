﻿using UnityEngine;
using Assets.Scripts.Sound;
using Assets.Scripts.App;
using System;

namespace Assets.Scripts.LevelCompleted
{
    public class LevelCompletedController : MonoBehaviour
    {
        private static LevelCompletedController levelCompletedController;

        void Awake()
        {
            if (levelCompletedController == null) levelCompletedController = this;
            else if (levelCompletedController != this) Destroy(gameObject);
            SoundController.GetController().PlayLevelCompleteSound();
        }     

        //internal void RetryLvl()
        //{
        //    AppController.GetController().RestartGame();
        //}

        internal void MainMenu()
        {
            ViewController.GetController().LoadMainMenu();
        }

        internal void PlayClicSound()
        {
            SoundController.GetController().PlayClickSound();
        }

        internal static LevelCompletedController GetController()
        {
            return levelCompletedController;
        }

        internal void RetryLvl()
        {
            AppController.GetController().PlayCurrentGame();

        }
    }
}
