﻿using System;
using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cover {

    public class CoverScreen : MonoBehaviour {

        public CoverView coverView;

        public Text startText;
        public Text aboutText;

        void OnEnable()
        {
            UpdateTexts();
        }

        public void OnClickStartBtn()
        {
            coverView.ClickSound();
            CoverController.GetController().StartGame();
        }

        public void OnClickOxBtn(){
            coverView.ClickSound();
            coverView.ShowOx();
            gameObject.SetActive(false);
        }   

        public void OnClickAboutBtn(){
            coverView.ClickSound();
            coverView.ShowAbout();
            gameObject.SetActive(false);
        }       

        public void OnClickArgentineBtn()
        {
            
           coverView.ClickSound();
            SettingsController.GetController().SwitchLanguage(0);
            UpdateTexts();
        }    

        public void OnClickBritishBtn()
        {
            
            coverView.ClickSound();
            SettingsController.GetController().SwitchLanguage(1);
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    startText.text = "JUGAR";
                    aboutText.text = "Acerca de Calculandox";
                    break;
                default:
                    startText.text = "PLAY";
                    aboutText.text = "About Calculandox";
                    break;
            }
        }

        public void ClickSound()
        {
            coverView.ClickSound();
        }


    }
}
