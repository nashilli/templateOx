﻿using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cover
{

    public class AboutScreen : MonoBehaviour
    {
        public CoverView coverView;


        public Text nameLabel;
        public Text ageRangeLabel;
        public Text areaLabel;
        public Text areaText;
        public Text contentsLabel;
        public Text contentsText;
        public Text creditsLabel;

        void OnEnable()
        {
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    nameLabel.text = "NOMBRE";
                    ageRangeLabel.text = "RANGO DE EDAD";
                    areaLabel.text = "ÁREA";
                    areaText.text = "MATEMÁTICA";
                    contentsLabel.text = "CONTENIDOS QUE ABARCA";
                    contentsText.text = "TO DO";
                    creditsLabel.text = "CRÉDITOS";
                    break;
                default:
                    nameLabel.text = "NAME";
                    ageRangeLabel.text = "AGE RANGE";
                    areaLabel.text = "AREA";
                    areaText.text = "MATHS";
                    contentsLabel.text = "CONTENTS";
                    contentsText.text = "TO DO";
                    creditsLabel.text = "CREDITS";
                    break;
            }
        }

        public void OnClickAboutScreen(){
            coverView.ClickSound();
            coverView.ShowCoverScreen();
            gameObject.SetActive(false);
        }

        
    }
}