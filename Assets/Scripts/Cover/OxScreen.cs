﻿using Assets.Scripts.Settings;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Cover
{

    public class OxScreen : MonoBehaviour
    {
        public CoverView coverView;

        public Text description;
        public Text moreInformationLabel;
        public Text contactLabel;
        
        void OnEnable()
        {
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    description.text = "Ox es una empresa dedicada al desarrollo e implementación de herramientas de innovación tecnológicas para ser utilizadas en áreas educativas.";

                    moreInformationLabel.text = "MÁS INFORMACIÓN";
                    contactLabel.text = "CONTACTO";
                    break;
                default:
                    description.text = "Ox is a company that develops and applies technologically innovative tools for educational purposes.";
                    moreInformationLabel.text = "MORE INFORMATION";
                    contactLabel.text = "CONTACT";
                    break;
            }
        }

        public void OnClickOxLink()
        {
            coverView.ClickSound();
            Application.OpenURL("http://oxed.com.ar");
        }

        public void OnClickOxScreen()
        {
            coverView.ClickSound();
            coverView.ShowCoverScreen();
            gameObject.SetActive(false);
        }
               
    }
}