﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts.App;
using Assets.Scripts.Metrics;

namespace Assets.Scripts._Levels.AGame
{

    public class AGameController : LevelController
    {
        private static AGameController aGameController;
        public AGameView aGameView;
        private AGameModel aGameModel;
        private const int TO_END = 5;

        void Awake()
        {
            if (aGameController == null) aGameController = this;
            else if (aGameController != this) Destroy(this);
            InitGame();
        }

        public override void InitGame()
        {
            aGameModel = new AGameModel();
            aGameModel.StartGame();
            NextChallenge();
            MetricsController.GetController().GameStart();
        }

        public override void NextChallenge()
        {
            aGameModel.NextChallenge();
            aGameView.NextChallenge(aGameModel.GetCurrentAnswer());
        }

        internal void CheckAnswer(int answer)
        {
            if (aGameModel.CheckAnswer(answer))
            {
                CorrectAnswer();
            } else
            {
                WrongAnswer();
            }
        }

        private void WrongAnswer()
        {
            LogAnswer(false);
            aGameView.WrongAnswer();
        }

        private void CorrectAnswer()
        {
            LogAnswer(true);
            aGameView.CorrectAnswer();
            if (GameIsEnded())
            {
                EndGame(aGameModel.GetMinSeconds(), aGameModel.GetPointsPerSecond(), aGameModel.GetPointsPerError());
            } else
            {
                NextChallenge();
            }
            
        }

        private bool GameIsEnded()
        {
            return GetCurrentMetrics().GetRightAnswers() == TO_END;
        }      

        public static AGameController GetController()
        {
            return aGameController;
        }

        public override void RestartGame()
        {
            MetricsController.GetController().GameStart();
            aGameModel.RestartGame();
            aGameView.RestartGame();
            NextChallenge();

        }
    }
}
