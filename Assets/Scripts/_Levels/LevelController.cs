﻿using Assets.Scripts.App;
using Assets.Scripts.Metrics;
using UnityEngine;

namespace Assets.Scripts._Levels
{

    public abstract class LevelController : MonoBehaviour
    {

        public abstract void NextChallenge();
        public abstract void InitGame();
        public abstract void RestartGame();

        public void LogAnswer(bool isCorrect)
        {
            MetricsController.GetController().LogAnswer(isCorrect);
        }    

        public void LogMetrics(int lapsedSeconds, int minSeconds, int pointsPerSecond, int pointsPerError)
        {
            MetricsController.GetController().GameFinished(minSeconds, pointsPerSecond, pointsPerError);
        }
        
        public void EndGame(int minSeconds, int pointsPerSecond, int pointsPerError)
        {
            MetricsController.GetController().GameFinished(minSeconds, pointsPerSecond, pointsPerError);
            ViewController.GetController().LoadLevelCompleted();
        }

        public GameMetrics GetCurrentMetrics()
        {
            return MetricsController.GetController().GetCurrentMetrics();
        }
    }
}
