﻿using System;
using Assets.Scripts.Settings;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Sound;
using Assets.Scripts.App;

namespace Assets.Scripts.Instructions
{
    public class GameRules : MonoBehaviour
    {

        [SerializeField]
        private Text backToGameLabel;
  

        void Start(){
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            switch (SettingsController.GetController().GetLanguage())
            {
                case 0:
                    backToGameLabel.text = "Volver al juego";
                    break;
                default:
                    backToGameLabel.text = "Back to game";
                    break;
            }
        }

        public void OnClickBackToGame()
        {
            ClickSound();
            ViewController.GetController().HideInstructions();
        }

        private void ClickSound()
        {
            SoundController.GetController().PlayClickSound();
        }
    }
}