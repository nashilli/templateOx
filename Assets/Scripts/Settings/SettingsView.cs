﻿using Assets.Scripts.App;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Settings
{

    public class SettingsView : MonoBehaviour
    {
        public SwitchPlayerView switchPlayerView;
        public GeneralSettingsView generalSettingsView;

        internal void ShowSwitchPlayer()
        {
            switchPlayerView.gameObject.SetActive(true);
        }

        internal void CloseSettings()
        {
            ViewController.GetController().LoadMainMenu();
        }

        internal void ShowGeneralSettings()
        {
            generalSettingsView.gameObject.SetActive(true);
        }
    }
}
